Run tests:
```sh
./mvnw clean test
```

To see test coverage report open (after running tests):
``
/target/site/jacoco/index.html
``

Start rest api (on localhost:8080):
```sh
./mvnw spring-boot:run
```