package me.sherafgan.transactions_task;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TransactionsTaskApplicationTests {

    @Test
    void contextLoads() {
        TransactionsTaskApplication.main(new String[]{});
    }

}
