package me.sherafgan.transactions_task.service;

import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import me.sherafgan.transactions_task.exception.TransferException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class TransferServiceTest {
    public static final String FROM_USER = "fromUser";
    public static final String TO_USER = "toUser";
    public static final String TEST_PASSWORD = "testPassword";

    public static final Integer FROM_ACCOUNT_BALANCE = 13001;
    public static final Integer TO_ACCOUNT_BALANCE = 2000;

    @Autowired
    private TransferService transferService;
    @Autowired
    private UserService userService;

    @BeforeEach
    void setUp() {
        List<Account> validAccounts = new ArrayList<>();
        User fromUser = new User(FROM_USER, TEST_PASSWORD, validAccounts);
        validAccounts.add(new Account(fromUser, FROM_ACCOUNT_BALANCE));
        userService.createAccounts(fromUser);
        validAccounts = new ArrayList<>();
        User toUser = new User(TO_USER, TEST_PASSWORD, validAccounts);
        validAccounts.add(0, new Account(toUser, TO_ACCOUNT_BALANCE));
        userService.createAccounts(toUser);
    }

    @AfterEach
    void tearDown() {
        if (userService.userExists(FROM_USER)) {
            userService.deleteUser(userService.getUser(FROM_USER, TEST_PASSWORD));
        }
        if (userService.userExists(TO_USER)) {
            userService.deleteUser(userService.getUser(TO_USER, TEST_PASSWORD));
        }
    }

    @Test
    void transferMoney_successful() {
        int amount = 200;
        transferService.transferMoney(userService.getUser(FROM_USER, TEST_PASSWORD).getAccounts().get(0),
                userService.getUser(TO_USER, TEST_PASSWORD).getAccounts().get(0),
                amount);
        assertEquals(FROM_ACCOUNT_BALANCE - amount,
                userService.getUser(FROM_USER, TEST_PASSWORD).getAccounts().get(0).getBalance());
        assertEquals(TO_ACCOUNT_BALANCE + amount,
                userService.getUser(TO_USER, TEST_PASSWORD).getAccounts().get(0).getBalance());
    }

    @Test
    void transferMoney_insufficientFunds_fail() {
        assertThrows(TransferException.class,
                () -> transferService.transferMoney(userService.getUser(FROM_USER, TEST_PASSWORD).getAccounts().get(0),
                        userService.getUser(TO_USER, TEST_PASSWORD).getAccounts().get(0),
                        FROM_ACCOUNT_BALANCE + 100),
                TransferService.INSUFFICIENT_FUNDS);
    }

    @Test
    void transferMoney_accountsDontExist_fail() {
        Account account = userService.getUser(TO_USER, TEST_PASSWORD).getAccounts().get(0);
        assertThrows(TransferException.class,
                () -> transferService.transferMoney(new Account(), account, 200),
                TransferService.ACCOUNTS_DO_NOT_EXIST);
        assertThrows(TransferException.class,
                () -> transferService.transferMoney(account, new Account(), 200),
                TransferService.ACCOUNTS_DO_NOT_EXIST);
    }

    @Test
    void transferMoney_maxAccountBalanceReached_fail() {
        assertThrows(TransferException.class,
                () -> transferService.transferMoney(userService.getUser(FROM_USER, TEST_PASSWORD).getAccounts().get(0),
                        userService.getUser(TO_USER, TEST_PASSWORD).getAccounts().get(0),
                        FROM_ACCOUNT_BALANCE),
                Account.MAX_BALANCE_MESSAGE);
    }
}