package me.sherafgan.transactions_task.service;

import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import me.sherafgan.transactions_task.exception.UserNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {
    public static final String TEST_USERNAME = "testUsername";
    public static final String TEST_PASSWORD = "testPassword";
    public static final String VALID_USERNAME = "validUsername";
    public static final String VALID_PASSWORD = "validPassword";

    @Autowired
    private UserService userService;

    @BeforeEach
    void setUp() {
        List<Account> validAccounts = new ArrayList<>();
        User newUser = new User(TEST_USERNAME, TEST_PASSWORD, validAccounts);
        validAccounts.add(new Account(newUser, 2031));
        userService.createAccounts(newUser);
    }

    @AfterEach
    void tearDown() throws UserNotFoundException {
        if (userService.userExists(TEST_USERNAME)) {
            userService.deleteUser(userService.getUser(TEST_USERNAME, TEST_PASSWORD));
        }
    }

    @Test
    void createAccount_invalidPassword_fail() {
        List<Account> validAccounts = new ArrayList<>();
        User newUser = new User(VALID_USERNAME, "tooShort", validAccounts);
        validAccounts.add(new Account(newUser, 2031));
        assertThrows(Exception.class, () -> {
            userService.createAccounts(newUser);
        });
        User newUser2 = new User(VALID_USERNAME, "tooLongPasswordIsConstrained", validAccounts);
        validAccounts.add(0, new Account(newUser, 2031));
        assertThrows(Exception.class, () -> {
            userService.createAccounts(newUser2);
        });
    }

    @Test
    void createAccount_nullUsername_fail() {
        List<Account> validAccounts = new ArrayList<>();
        User newUser = new User(null, VALID_PASSWORD, validAccounts);
        validAccounts.add(new Account(newUser, 2031));
        assertThrows(Exception.class,
                () -> userService.createAccounts(newUser));
    }

    @Test
    void createAccount_accountBalancesEmptyOrNull_fail() {
        assertThrows(Exception.class,
                () -> userService.createAccounts(new User(VALID_USERNAME + "01", VALID_PASSWORD, Collections.emptyList())));
        assertThrows(Exception.class,
                () -> userService.createAccounts(new User(VALID_USERNAME + "01", VALID_PASSWORD, null)));
    }

    @Test
    void userExists_successful() {
        assertTrue(userService.userExists(TEST_USERNAME));
    }

    @Test
    void getUser_successful() {
        User user = userService.getUser(TEST_USERNAME, TEST_PASSWORD);
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getUsername());
        assertEquals(TEST_PASSWORD, user.getPassword());
        List<Account> accounts = user.getAccounts();
        assertEquals(1, accounts.size());
    }

    @Test
    void getUser_wrongCredentials_fail() {
        assertThrows(UserNotFoundException.class,
                () -> userService.getUser("randomUsername", "randomPassword"),
                UserService.USER_NOT_FOUND);
        assertThrows(UserNotFoundException.class,
                () -> userService.getUser(TEST_USERNAME, "randomPassword"),
                UserService.USER_NOT_FOUND);
    }

    @Test
    void getAccountsBalances_successful() {
        User user = userService.getUser(TEST_USERNAME, TEST_PASSWORD);
        List<Account> accounts = userService.getAccounts(user.getId());
        assertEquals(1, accounts.size());
        assertEquals(2031, accounts.get(0).getBalance());
    }

    @Test
    void getAccounts_nonexistentAccount_fail() {
        assertThrows(UserNotFoundException.class, () -> userService.getAccounts(Integer.MAX_VALUE), UserService.USER_NOT_FOUND);
    }

    @Test
    void deleteUser_Successful() {
        userService.deleteUser(userService.getUser(TEST_USERNAME, TEST_PASSWORD));
        assertThrows(UserNotFoundException.class, () -> userService.getUser(TEST_USERNAME, TEST_PASSWORD));
    }
}