package me.sherafgan.transactions_task.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import me.sherafgan.transactions_task.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {
    public static final String TEST_USERNAME = "testUsername";
    public static final String TEST_PASSWORD = "testPassword";
    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    private User user;
    private String userJson;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        List<Account> validAccountBalances = new ArrayList<>();
        user = new User(TEST_USERNAME, TEST_PASSWORD, validAccountBalances);
        validAccountBalances.add(new Account(user, 2500));
        validAccountBalances.add(new Account(user, 1400));
        userJson = new ObjectMapper().writeValueAsString(user);
    }

    @Test
    void createAccounts_successful() throws Exception {
        when(userService.createAccounts(any(User.class))).thenReturn(user);
        User returnedUser = new ObjectMapper().readValue(
                checkEndpointAccounts(checkEndpointUser(checkEndpoint(post("/createAccounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(userJson)
                        .accept(MediaType.APPLICATION_JSON))), false),
                User.class);
        checkUser(returnedUser);
        checkAccounts(returnedUser.getAccounts());
    }

//    @Test
//    void getUser_successful() throws Exception {
//        when(userService.getUser(any(String.class), any(String.class))).thenReturn(user);
//        User returnedUser = new ObjectMapper().readValue(
//                checkEndpointAccounts(checkEndpointUser(checkEndpoint(get("/getUser")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .characterEncoding("UTF-8")
//                        .content(userJson)
//                        .accept(MediaType.APPLICATION_JSON))), false),
//                User.class);
//        checkUser(returnedUser);
//        checkAccounts(returnedUser.getAccounts());
//    }

    @Test
    void getAccounts_successful() throws Exception {
        when(userService.getAccounts(any(Integer.class))).thenReturn(user.getAccounts());
        List<Account> returnedAccounts = new ObjectMapper().readValue(
                checkEndpointAccounts(checkEndpoint(get("/getAccounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .param("userId", String.valueOf(Integer.MAX_VALUE))
                        .accept(MediaType.APPLICATION_JSON)), true),
                new TypeReference<List<Account>>() {
                });
        checkAccounts(returnedAccounts);
    }

    private ResultActions checkEndpoint(MockHttpServletRequestBuilder method) throws Exception {
        return mockMvc.perform(method)
                .andDo(print())
                .andExpect(status().isOk());
    }

    private ResultActions checkEndpointUser(ResultActions resultActions) throws Exception {
        return resultActions
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").exists());
    }

    private String checkEndpointAccounts(ResultActions resultActions, boolean onlyAccounts) throws Exception {
        String jsonPathStartsWith = onlyAccounts ? "$." : "$.accounts.";
        return resultActions
                .andExpect(MockMvcResultMatchers.jsonPath(jsonPathStartsWith + "[0].id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath(jsonPathStartsWith + "[0].balance").exists())
                .andExpect(MockMvcResultMatchers.jsonPath(jsonPathStartsWith + "[1].id").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath(jsonPathStartsWith + "[1].balance").exists())
                .andReturn().getResponse().getContentAsString();
    }

    private void checkUser(User user) {
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getUsername());
        assertEquals(TEST_PASSWORD, user.getPassword());
    }

    private void checkAccounts(List<Account> accounts) {
        assertEquals(user.getAccounts().size(), accounts.size());
        assertEquals(user.getAccounts().get(0).getBalance(), accounts.get(0).getBalance());
        assertEquals(user.getAccounts().get(1).getBalance(), accounts.get(1).getBalance());
    }
}