package me.sherafgan.transactions_task.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.TransferDTO;
import me.sherafgan.transactions_task.service.TransferService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TransferController.class)
class TransferControllerTest {

    public static final Integer to = 3500;
    private static final Integer from = 2000;
    private static final Integer amount = 100;

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TransferService transferService;

    @Test
    void transferMoney_successful() throws Exception {
        TransferDTO requestDTO = new TransferDTO(new Account(null, from), new Account(null, to), amount);
        String requestJsonContent = new ObjectMapper().writeValueAsString(requestDTO);
        System.out.println(requestJsonContent);

        TransferDTO responseDTO = new TransferDTO(new Account(null, from - amount), new Account(null, to + amount), amount);

        when(transferService.transferMoney(any(Account.class), any(Account.class), any(Integer.class))).thenReturn(responseDTO);
        String responseContent = mockMvc.perform(
                post("/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(requestJsonContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.fromAccount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.toAccount").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").exists())
                .andReturn().getResponse().getContentAsString();
        TransferDTO responseContentDTO = new ObjectMapper().readValue(responseContent, TransferDTO.class);
        assertEquals(responseContentDTO.getFromAccount().getBalance(), from - amount);
        assertEquals(responseContentDTO.getToAccount().getBalance(), to + amount);

    }
}