package me.sherafgan.transactions_task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "BAD OR IMPOSSIBLE TRANSFER!")
public class TransferException extends RuntimeException {
    public TransferException(String message) {
        super(message);
    }
}
