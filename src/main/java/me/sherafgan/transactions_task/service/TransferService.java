package me.sherafgan.transactions_task.service;

import me.sherafgan.transactions_task.data.AccountRepository;
import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.TransferDTO;
import me.sherafgan.transactions_task.exception.TransferException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TransferService {
    public static final String ACCOUNTS_DO_NOT_EXIST = "One or all accounts specified do not exist!";
    public static final String INSUFFICIENT_FUNDS = "Insufficient funds!";

    private final AccountRepository accountRepository;

    public TransferService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public TransferDTO transferMoney(Account fromAccount, Account toAccount, Integer amount) {
        if (fromAccount.getBalance() == null || toAccount.getBalance() == null)
            throw new TransferException(ACCOUNTS_DO_NOT_EXIST);
        if (amount > fromAccount.getBalance()) throw new TransferException(INSUFFICIENT_FUNDS);
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        if ((toAccount.getBalance() + amount) > Account.MAX_BALANCE_ALLOWED)
            throw new TransferException(Account.MAX_BALANCE_MESSAGE);
        toAccount.setBalance(toAccount.getBalance() + amount);
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);
        return new TransferDTO(fromAccount, toAccount, amount);
    }
}
