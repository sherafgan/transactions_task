package me.sherafgan.transactions_task.service;

import me.sherafgan.transactions_task.data.AccountRepository;
import me.sherafgan.transactions_task.data.UserRepository;
import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import me.sherafgan.transactions_task.exception.AccountCreationException;
import me.sherafgan.transactions_task.exception.UserNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    public static final String USER_NOT_FOUND = "User with given credentials not found!";
    public static final String USER_NOT_CREATED = "User wasn't created!";

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;

    public UserService(UserRepository userRepository, AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public User createAccounts(User newUser) {
        userRepository.save(newUser);
        newUser.getAccounts().forEach(accountRepository::save);
        Optional<User> savedUser = userRepository.findByUsername(newUser.getUsername());
        if (!savedUser.isPresent()) {
            throw new AccountCreationException(USER_NOT_CREATED);
        } else {
            return savedUser.get();
        }
    }

    public boolean userExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }


    public User getUser(String username, String password) {
        Optional<User> foundUser = userRepository.findByUsername(username);
        if (foundUser.isPresent() && foundUser.get().getPassword().equals(password)) {
            return foundUser.get();
        }
        throw new UserNotFoundException(USER_NOT_FOUND);
    }

    public List<Account> getAccounts(Integer userId) {
        Optional<User> targetUser = userRepository.findById(userId);
        if (!targetUser.isPresent()) throw new UserNotFoundException(USER_NOT_FOUND);
        return targetUser.get().getAccounts();
    }

    @Transactional(rollbackOn = Exception.class)
    public void deleteUser(User user) {
        List<Account> userAccounts = accountRepository.findAccountsByUser(user);
        userAccounts.forEach(accountRepository::delete);
        userRepository.delete(user);
    }
}
