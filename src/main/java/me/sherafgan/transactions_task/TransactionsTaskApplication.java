package me.sherafgan.transactions_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionsTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionsTaskApplication.class, args);
	}

}
