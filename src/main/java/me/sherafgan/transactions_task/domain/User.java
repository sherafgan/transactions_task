package me.sherafgan.transactions_task.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@Entity
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private String username;
    @Setter
    @Size(min = 10, max = 25, message = "Password length cannot be < 10 and > 25 characters!")
    private String password;
    @Setter
    @OneToMany(fetch = FetchType.EAGER)
    @JsonManagedReference
    @Size(min = 1, message = "A user should at least have one account!")
    private List<Account> accounts;

    public User(String username, String password, List<Account> accounts) {
        this.username = username;
        this.password = password;
        this.accounts = accounts;
    }

    public User(UserDTO userDTO) {
        this.username = userDTO.getUsername();
        this.password = userDTO.getPassword();
        this.accounts = userDTO.getAccounts();
    }
}
