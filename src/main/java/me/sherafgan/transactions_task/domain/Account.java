package me.sherafgan.transactions_task.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;

@NoArgsConstructor
@Entity
@Getter
public class Account {
    public static final int MAX_BALANCE_ALLOWED = 15000;
    public static final String MAX_BALANCE_MESSAGE = "One account can only hold a balance of " + MAX_BALANCE_ALLOWED + " dollars!";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private User user;
    @Setter
    @Max(value = MAX_BALANCE_ALLOWED, message = MAX_BALANCE_MESSAGE)
    private Integer balance;

    public Account(User user, Integer balance) {
        this.user = user;
        this.balance = balance;
    }
}
