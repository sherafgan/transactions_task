package me.sherafgan.transactions_task.domain;

import lombok.Getter;

import java.util.List;

@Getter
public class UserDTO {
    private String username;
    private String password;
    private List<Account> accounts;

    public UserDTO(String username, String password, List<Account> accounts) {
        this.username = username;
        this.password = password;
        this.accounts = accounts;
    }
}
