package me.sherafgan.transactions_task.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class TransferDTO {
    private Account fromAccount;
    private Account toAccount;
    private Integer amount;

    public TransferDTO(Account fromAccount, Account toAccount, Integer amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }
}
