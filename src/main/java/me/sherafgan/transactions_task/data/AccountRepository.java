package me.sherafgan.transactions_task.data;

import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepository extends CrudRepository<Account, Integer> {
    List<Account> findAccountsByUser(User user);
}
