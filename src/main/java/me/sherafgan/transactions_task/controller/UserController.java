package me.sherafgan.transactions_task.controller;

import me.sherafgan.transactions_task.domain.Account;
import me.sherafgan.transactions_task.domain.User;
import me.sherafgan.transactions_task.domain.UserDTO;
import me.sherafgan.transactions_task.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/createAccounts",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public User createAccounts(@RequestBody UserDTO userDTO) {
        User newUser = new User(userDTO);
        return userService.createAccounts(newUser);
    }

    @GetMapping(value = "/getAccounts")
    public List<Account> getBalanceInfo(@RequestParam Integer userId) {
        return userService.getAccounts(userId);
    }
}
