package me.sherafgan.transactions_task.controller;

import me.sherafgan.transactions_task.domain.TransferDTO;
import me.sherafgan.transactions_task.service.TransferService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransferController {
    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping(value = "/transfer")
    public TransferDTO transferMoney(@RequestBody TransferDTO transferDTO) {
        return transferService.transferMoney(transferDTO.getFromAccount(), transferDTO.getToAccount(), transferDTO.getAmount());
    }
}
